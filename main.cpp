/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * main.cpp, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "util.h"
#include "SudokuBoard.h"
#include "SudokuBoardEditor.h"
#include "git_version.h"

#define APP_NAME_STRING "Sudoku Solver"
#define APP_VERSION_STRING "0.1.0"
#define INPUT_BOARD_ROW 2
#define INPUT_BOARD_COL 0
#define OUTPUT_BOARD_ROW 2
#define OUTPUT_BOARD_COL 25


/** \brief
 *
 * \param w WINDOW*
 * \param y int
 * \param x int
 * \param b SudokuBoard*
 * \return void
 *
 */
static void print_sudoku_board(WINDOW *w, int y, int x, SudokuBoard *b)
{
    for(int i=0; i<9; ++i) {
        for(int j=0; j<9; ++j) {
            mvwaddch(w, y+i*2, x+j*2, '0'+b->get(i,j));
        }
    }
    refresh();
}

/** \brief
 *
 * \param privdata void*
 * \param b class SudokuBoard*
 * \return void
 *
 */
static void handle_board_notify(void *privdata, class SudokuBoard *b)
{
    WINDOW *screen = static_cast<WINDOW *>(privdata);
    print_sudoku_board(screen, OUTPUT_BOARD_ROW, OUTPUT_BOARD_COL, b);
}

/** \brief
 *
 * \param argc int
 * \param argv char**
 * \return int
 *
 */
int main(int argc, char **argv)
{
    WINDOW *screen = initscr();
    raw();
    clear();
    noecho();
    cbreak();
    keypad(screen, TRUE);

    mvwprintw(screen, 0, 0, APP_NAME_STRING " v" APP_VERSION_STRING " / Git build: " GIT_VERSION);

    SudokuBoard b;
    b.registerForNotifications(handle_board_notify, (void *)screen);
    SudokuBoardEditor e(screen, INPUT_BOARD_ROW, INPUT_BOARD_COL, &b);
    while (!e.doneEditing()) {
        int ch = getch();
        e.doKeyPress(ch);
    }

    if (!e.quitNow()) {
        b.solve();
        mvwprintw(screen, 18, 50, "DONE");
        getch();
    }

    endwin();
    screen = nullptr;
    return 0;
}
