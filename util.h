/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * util.h, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED

#ifdef USE_COLORS
#define SET_COLOR_RED    "\033[31m"
#define SET_COLOR_CYAN   "\033[36m"
#define SET_COLOR_YELLOW "\033[33m"
#define SET_COLOR_PURPLE "\033[35m"
#define RESET_COLOR      "\033[0m"
#else
#define SET_COLOR_RED    ""
#define SET_COLOR_CYAN   ""
#define SET_COLOR_YELLOW ""
#define SET_COLOR_PURPLE ""
#define RESET_COLOR      ""
#endif

/*! \brief Acts like printf, execpt that it exits after printing to stderr
 *         with whatever value you put in err. And it prints the message in red.
 *
 * \param err int
 * \param fmt const char*
 * \param ...
 * \return [[noreturn]] void
 *
 */
void panic(const char *fmt, ...);

/*! \brief Acts like printf, except it prints in yellow :P
 *
 * \param fmt const char*
 * \param ...
 * \return void
 *
 */
void info(const char *fmt, ...);

/*! \brief If expr is true, the call does nothing. If expr is false, then it acts
 *         like panic() above, except it always exits with code -1
 *
 * \param expr bool
 * \param fmt const char*
 * \param ...
 * \return void
 *
 */
void my_assert(bool expr, const char *fmt, ...);

#endif // UTIL_H_INCLUDED
