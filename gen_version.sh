#!/bin/bash

# Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
# gen_version.sh, part of sudoku-solver
#
# sudoku-solver is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.
#
# sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with sudoku-solver.
# If not, see <https://www.gnu.org/licenses/>.
#
 
echo "#ifndef GIT_VERSION_H" > git_version.h
echo "#define GIT_VERSION_H" >> git_version.h
echo "#define GIT_VERSION \"`git describe --always --dirty --tags`\"" >> git_version.h
echo "#define BUILD_DATE \"`date`\"" >> git_version.h
echo "#endif // GIT_VERSION_H" >> git_version.h
