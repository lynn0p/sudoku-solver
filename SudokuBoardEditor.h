/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * SudokuBoardEditor.h, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SUDOKUBOARDEDITOR_H
#define SUDOKUBOARDEDITOR_H

#include <ncurses.h>
#include "SudokuBoard.h"

class SudokuBoardEditor
{
    public:
        SudokuBoardEditor(WINDOW *w, int r_offset, int c_offset, SudokuBoard *b);
        virtual ~SudokuBoardEditor();

        void doKeyPress(int key);
        bool doneEditing();
        bool quitNow();

    private:
        WINDOW      *m_screen;
        int          m_row_offset;
        int          m_col_offset;
        SudokuBoard *m_board;
        int          m_row;
        int          m_col;
        bool         m_done;
        bool         m_quit;

        void print_sudoku_board(int y, int x);
        void moveCursorUp();
        void moveCursorDown();
        void moveCursorLeft();
        void moveCursorRight();
        void setBoardValue(int key);
        void tabMove();
        void backtabMove();
};

#endif // SUDOKUBOARDEDITOR_H
