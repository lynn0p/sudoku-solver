/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * SudokuBoardEditor.cpp, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include "SudokuBoardEditor.h"
#include "util.h"

/** \brief
 *
 * \param w WINDOW*
 * \param r_offset int
 * \param c_offset int
 * \param b SudokuBoard*
 *
 */
SudokuBoardEditor::SudokuBoardEditor(WINDOW *w, int r_offset, int c_offset, SudokuBoard *b) :
    m_screen(w), m_row_offset(r_offset), m_col_offset(c_offset),
    m_board(b), m_row(0), m_col(0), m_done(false), m_quit(false)
{
    print_sudoku_board(m_row_offset, m_col_offset);
    move(m_row_offset, m_col_offset);
}

/** \brief
 */
SudokuBoardEditor::~SudokuBoardEditor()
{
    m_board = nullptr;
    m_screen = nullptr;
}

/** \brief
 *
 * \param key int
 * \return void
 *
 */
void SudokuBoardEditor::doKeyPress(int key)
{
    switch (key) {
        case KEY_UP:
            moveCursorUp();
            break;
        case KEY_DOWN:
            moveCursorDown();
            break;
        case KEY_LEFT:
            moveCursorLeft();
            break;
        case KEY_RIGHT:
            moveCursorRight();
            break;
        case ' ':
        case '\t':
            tabMove();
            break;
        case KEY_BTAB:
            backtabMove();
            break;
        case '\n':
            m_done = true;
            break;
        case 'q':
            m_done = true;
            m_quit = true;
            break;
        default:
            if (isdigit(key)) {
                setBoardValue(key);
                tabMove();
                print_sudoku_board(m_row_offset, m_col_offset);
            }
    }
}

/** \brief
 *
 * \return bool
 *
 */
bool SudokuBoardEditor::doneEditing()
{
    return m_done;
}

/** \brief
 *
 * \return bool
 *
 */
bool SudokuBoardEditor::quitNow()
{
    return m_quit;
}

/** \brief
 *
 * \param y int
 * \param x int
 * \return void
 *
 */
void SudokuBoardEditor::print_sudoku_board(int y, int x)
{
    for(int i=0; i<9; ++i) {
        for(int j=0; j<9; ++j) {
            mvwaddch(m_screen, y+i*2, x+j*2, '0'+m_board->get(i,j));
        }
    }
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::moveCursorUp()
{
    m_row = (m_row - 1) < 0 ? 8 : m_row - 1;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::moveCursorDown()
{
    m_row = (m_row + 1) % 9;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::moveCursorLeft()
{
    m_col = (m_col - 1) < 0 ? 8 : m_col - 1;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::moveCursorRight()
{
    m_col = (m_col + 1) % 9;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \param key int
 * \return void
 *
 */
void SudokuBoardEditor::setBoardValue(int key)
{
    int num = key - '0';
    m_board->set(m_row, m_col, num);
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::tabMove()
{
    int next_col = (m_col + 1) % 9;
    int next_row = m_row;
    if (next_col == 0) {
        next_row = (m_row + 1) % 9;
    }
    m_col = next_col;
    m_row = next_row;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoardEditor::backtabMove()
{
    int next_col = m_col - 1;
    int next_row = m_row;
    if (next_col < 0) {
        next_col = 8;
        next_row = (m_row - 1) < 0 ? 8 : m_row - 1;
    }
    m_row = next_row;
    m_col = next_col;
    move(m_row_offset+m_row*2, m_col_offset+m_col*2);
    refresh();
}
