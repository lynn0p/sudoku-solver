/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * util.cpp, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>

#if defined(LINUX)
#include <ncurses.h>
#elif defined(WIN32)
#include <curses.h>
#else
#error "Need to include a curses header for your platform or define the right macro"
#endif

#include "util.h"


using namespace std;

/*! \brief
 *
 * \param buf const char*
 * \return void
 *
 */
static void panic(void *buf)
{
	fprintf(stderr, "%s%s%s\n", SET_COLOR_RED, (char *)buf, RESET_COLOR);
	endwin();

#if defined(LINUX)
	raise(SIGTRAP); // this stops the program or drops into debugger
#elif defined(WIN32)
    DebugBreak();
#else
#error "Need to port panic() to your platform or define the right macro"
#endif
}

/*! \brief Basically, an unconditional version of my_assert()
 *
 * \param err int
 * \param fmt const char*
 * \param ...
 * \return void
 *
 */
void panic(const char *fmt, ...)
{
	char buf[1024];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf)-1, fmt, args);
	va_end(args);
	panic((void *)buf);
}

/*! \brief Acts like printf, except it prints in yellow :P
 *
 * \param fmt const char*
 * \param ...
 * \return void
 *
 */
void info(const char *fmt, ...)
{
	char buf[1024];
	va_list args;
	va_start(args, fmt);
	vsnprintf(buf, sizeof(buf)-1, fmt, args);
	va_end(args);
	fprintf(stderr, "%s%s%s\n", SET_COLOR_YELLOW, buf, RESET_COLOR);
}


/*! \brief If expr is true, the call does nothing. If expr is false, then it acts
 *         like panic() above, but raises  SIGTRAP which causes the program to halt.
 *         if you have a debugger attached, it should drop you into it
 *
 * \param expr bool
 * \param fmt const char*
 * \param ...
 * \return void
 *
 */
void my_assert(bool expr, const char *fmt, ...)
{
    if (expr == false) {
        char buf[1024];
        va_list args;
        va_start(args, fmt);
        vsnprintf(buf, sizeof(buf)-1, fmt, args);
        va_end(args);
        panic((void *)buf);
    }
}
