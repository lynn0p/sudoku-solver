# If you want to use LLVM clang++, uncomment the following line and modify it
CXX = clang++-15

CXXFLAGS = -Wall -O2 -DUSE_COLORS -DLINUX
OBJS = main.o util.o SudokuBoard.o SudokuBoardEditor.o
HDRS = util.h SudokuBoard.h SudokuBoardEditor.h git_version.h
LIBS = -lncurses
APPNAME = sudoku-solver

all: $(APPNAME)

git_version.h:
	./gen_version.sh

main.o: main.cpp $(HDRS)
util.o: util.cpp $(HDRS)
SudokuBoard.o: SudokuBoard.cpp $(HDRS)
SudokuBoardEditor.o: SudokuBoardEditor.cpp $(HDRS)

$(APPNAME): $(OBJS) 
	$(CXX) -o $(APPNAME) $(OBJS) $(LIBS)

install: $(APPNAME)
	@if [ -d ${HOME}/bin ]; then cp $(APPNAME) ${HOME}/bin; fi

clean:
	rm -f *.o
	rm -f git_version.h
	rm -f $(APPNAME)

