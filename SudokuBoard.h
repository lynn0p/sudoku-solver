/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * SudokuBoard.h, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SUDOKUBOARD_H
#define SUDOKUBOARD_H


class SudokuBoard
{
    public:
        SudokuBoard();
        virtual ~SudokuBoard();
        SudokuBoard(const SudokuBoard& other);
        SudokuBoard& operator=(const SudokuBoard& other);

        typedef void (*NotifyCallback)(void *privdata, class SudokuBoard *b);

        unsigned char get(int row, int col);
        void          set(int row, int col, unsigned char num);
        void          solve();
        bool          isSolved();
        void          registerForNotifications(NotifyCallback cb, void *priv);

    protected:

    private:
        NotifyCallback  m_notify_cb;
        void           *m_cb_priv;
        unsigned char   m_tbl[9][9];

        bool UsedInRow(int row, unsigned char num);
        bool UsedInCol(int col, unsigned char num);
        bool UsedInBox(int boxStartRow, int boxStartCol, unsigned char num);
        bool isPlausible(int row, int col, unsigned char num);

        bool do_first_pass();
        bool find_first_zero(int &i, int &j);
        bool solve_main();
        void shuffle(unsigned char *vec);
};

#endif // SUDOKUBOARD_H
