sudoku-solver
==========

Design:
=======
	Another lightweight curses based program. Is probably portable enough
	to compile and run on Windows, even. Does the basic recursive depth-first
	search for the sudoku solution. For most boards, this will complete
	fairly quick but there are cases out there where this will sit and spin
	way longer than it would take you to solve it.

What it does:
=============
	When it starts up, it starts up in edit mode. Enter the board from
	the newspaper or the internet or your favorite sudoku puzzle generator
	using the arrow keys, the tab key and the spacebar can all be used to
	navigate around the board. When you're done hit Enter and it will
	start to solve the  board. When it has found a solution it will print
	out DONE and halt.
	
To build:
=========
	0. Make sure some sort of *curses library is accessible. pdcurses
	on Windows has been verified to compile and link but I haven't been
	motivated enough to get it to run. Wants the dll on some search path
	or something. That should be the only dependency this code has, the
	rest is portable C++
	1. Obtain a recent version of Code::Blocks IDE
	2. Open codeblocks/sudoku-solver.cbp within Code::Blocks and hit Build
    
    or
    
    1. Obtain a recent version of Codelite IDE
    2. Open codelite/sudoku-solver.workspace within Codelite and hit Build
    
    or
    
	1. Type "make clean; make". Make sure you have a GNU C++ compiler
	   installed. The LLVM clang++-15 compiler has been tested and the
	   code compiles cleanly and seems to run OK. Look at the Makefile
	   and uncomment the lines indicated.
