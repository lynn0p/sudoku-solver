/* Copyright 2023 Owen Lynn (owen.lynn@hotmail.com)
 * SudokuBoard.cpp, part of sudoku-solver
 *
 * sudoku-solver is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * sudoku-solver is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with sudoku-solver.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include <vector>
#include <string.h>
#include <stdlib.h>
#include "SudokuBoard.h"
#include "util.h"

#define NOTIFY(X,Y,Z) if (X) { X(Y,Z); }

using namespace std;

/** \brief
 */
SudokuBoard::SudokuBoard()
{
    m_notify_cb = nullptr;
    m_cb_priv = nullptr;
    memset(m_tbl, 0, sizeof(m_tbl));
}

/** \brief
 */
SudokuBoard::~SudokuBoard()
{
}

/** \brief
 *
 * \param other const SudokuBoard&
 *
 */
SudokuBoard::SudokuBoard(const SudokuBoard& other)
{
    m_notify_cb = other.m_notify_cb;
    m_cb_priv = other.m_cb_priv;
    for(int i=0; i<9; ++i) {
        for(int j=0; j<9; ++j) {
            m_tbl[i][j] = other.m_tbl[i][j];
        }
    }
}

/** \brief
 *
 * \param rhs const SudokuBoard&
 * \return SudokuBoard&
 *
 */
SudokuBoard& SudokuBoard::operator=(const SudokuBoard& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    m_notify_cb = rhs.m_notify_cb;
    m_cb_priv = rhs.m_cb_priv;
    for(int i=0; i<9; ++i) {
        for(int j=0; j<9; ++j) {
            m_tbl[i][j] = rhs.m_tbl[i][j];
        }
    }
    return *this;
}

/** \brief
 *
 * \param row int
 * \param col int
 * \return unsigned char
 *
 */
unsigned char SudokuBoard::get(int row, int col)
{
    unsigned char c = 0;
    if(row>-1 && row<9 && col>-1 && col<9) {
        c = m_tbl[row][col];
    } else {
        panic("SudokuBoard::get() values out of range, row = %d, col = %d\n", row, col);
    }
    return c;
}

/** \brief
 *
 * \param row int
 * \param col int
 * \param num unsigned char
 * \return void
 *
 */
void SudokuBoard::set(int row, int col, unsigned char num)
{
    if(row>-1 && row<9 && col>-1 && col<9) {
        m_tbl[row][col] = num;
    } else {
        panic("SudokuBoard::set() values out of range, row = %d, col = %d\n", row, col);
    }
}

/** \brief
 *
 * \return void
 *
 */
void SudokuBoard::solve()
{
    NOTIFY(m_notify_cb,m_cb_priv,this);
    while (do_first_pass()) {}
    NOTIFY(m_notify_cb,m_cb_priv,this);
    solve_main();
    NOTIFY(m_notify_cb,m_cb_priv,this);
}

/** \brief
 *
 * \return bool
 *
 */
bool SudokuBoard::isSolved()
{
    int i,j;
    return !find_first_zero(i,j);
}

/** \brief
 *
 * \param cb NotifyCallback
 * \param priv void*
 * \return void
 *
 */
void SudokuBoard::registerForNotifications(NotifyCallback cb, void *priv)
{
    m_notify_cb = cb;
    m_cb_priv = priv;
}


/** \brief
 *
 * \param row int
 * \param num unsigned char
 * \return bool
 *
 */
inline bool SudokuBoard::UsedInRow(int row, unsigned char num)
{
    return (
        m_tbl[row][0] == num ||
        m_tbl[row][1] == num ||
        m_tbl[row][2] == num ||
        m_tbl[row][3] == num ||
        m_tbl[row][4] == num ||
        m_tbl[row][5] == num ||
        m_tbl[row][6] == num ||
        m_tbl[row][7] == num ||
        m_tbl[row][8] == num
    );
}

/** \brief
 *
 * \param col int
 * \param num unsigned char
 * \return bool
 *
 */
inline bool SudokuBoard::UsedInCol(int col, unsigned char num)
{
    return (
        m_tbl[0][col] == num ||
        m_tbl[1][col] == num ||
        m_tbl[2][col] == num ||
        m_tbl[3][col] == num ||
        m_tbl[4][col] == num ||
        m_tbl[5][col] == num ||
        m_tbl[6][col] == num ||
        m_tbl[7][col] == num ||
        m_tbl[8][col] == num
    );}

/** \brief
 *
 * \param i int
 * \param j int
 * \param num unsigned char
 * \return bool
 *
 */
inline bool SudokuBoard::UsedInBox(int i, int j, unsigned char num)
{
    return (
        m_tbl[i+0][j+0] == num || m_tbl[i+0][j+1] == num || m_tbl[i+0][j+2] == num ||
        m_tbl[i+1][j+0] == num || m_tbl[i+1][j+1] == num || m_tbl[i+1][j+2] == num ||
        m_tbl[i+2][j+0] == num || m_tbl[i+2][j+1] == num || m_tbl[i+2][j+2] == num
    );
}

/** \brief
 *
 * \param row int
 * \param col int
 * \param num unsigned char
 * \return bool
 *
 */
inline bool SudokuBoard::isPlausible(int row, int col, unsigned char num)
{
    return (
        m_tbl[row][col] == 0 &&
        !UsedInRow(row, num) &&
        !UsedInCol(col, num) &&
        !UsedInBox(row - row % 3, col - col % 3, num)
    );
}

/** \brief
 *
 * \return bool
 *
 */
bool SudokuBoard::do_first_pass()
{
    bool rc = false;
    for(int i=0; i<9;  ++i) {
        for(int j=0; j<9; ++j) {
            vector<unsigned char> foo;
            if (isPlausible(i, j, 1)) { foo.push_back(1); }
            if (isPlausible(i, j, 2)) { foo.push_back(2); }
            if (isPlausible(i, j, 3)) { foo.push_back(3); }
            if (isPlausible(i, j, 4)) { foo.push_back(4); }
            if (isPlausible(i, j, 5)) { foo.push_back(5); }
            if (isPlausible(i, j, 6)) { foo.push_back(6); }
            if (isPlausible(i, j, 7)) { foo.push_back(7); }
            if (isPlausible(i, j, 8)) { foo.push_back(8); }
            if (isPlausible(i, j, 9)) { foo.push_back(9); }
            if (foo.size() == 1) {
                m_tbl[i][j] = foo[0];
                rc = true;
            }
        }
    }
    return rc;
}

/** \brief
 *
 * \param i int&
 * \param j int&
 * \return bool
 *
 */
bool SudokuBoard::find_first_zero(int &i, int &j)
{
    for(i=0; i<9; ++i) {
        for(j=0; j<9; ++j) {
            if (0 == m_tbl[i][j]) { return true; }
        }
    }
    return false;
}

/** \brief
 *
 * \return bool
 *
 */
bool SudokuBoard::solve_main()
{
    int i, j;
    if (!find_first_zero(i, j)) {
        NOTIFY(m_notify_cb,m_cb_priv,this);
        // board is filled
        return true;
    }

    unsigned char vec[9] = {1,2,3,4,5,6,7,8,9};
    shuffle(vec);
    for(int k=0; k<9; ++k) {
        if (isPlausible(i,j,vec[k])) {
            m_tbl[i][j] = vec[k];
            NOTIFY(m_notify_cb,m_cb_priv,this);
            if (solve_main()) { return true; }
            m_tbl[i][j] = 0;
        }
    }

    NOTIFY(m_notify_cb,m_cb_priv,this);
    return false;
}

/** \brief
 *
 * \param vec unsigned char*
 * \return void
 *
 */
void SudokuBoard::shuffle(unsigned char *vec)
{
    for (int i=8; i>=1; --i) {
        int j = rand() % i;
        unsigned char temp = vec[i];
        vec[i] = vec[j];
        vec[j] = temp;
    }
}
